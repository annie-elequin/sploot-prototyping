/* eslint-disable no-useless-escape */
import { marked } from "marked";

export function shuffle(array: any[]) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export function processCardDescriptionMarkdown({
  value,
  regex,
  fontSize = 18,
  iconSize = 20,
  html = false,
}) {
  let ret = marked(value);

  const getIcon = (value) => {
    const type = value.charAt(1);
    const num = value.slice(2, -1);
    let adjustmentValue = num.length > 1 ? -5 : 0;
    if (html && num.length > 1) adjustmentValue += 5;
    switch (type) {
      case "d":
        return {
          iconSize: iconSize + 25,
          fontSize: fontSize + 4 + adjustmentValue,
        };

      case "p":
      case "b":
        return {
          iconSize: iconSize + 15,
          fontSize: fontSize + 4 + adjustmentValue,
        };
      case "o":
      case "h":
        return {
          iconSize: iconSize + 10,
          fontSize: fontSize + 3 + adjustmentValue,
        };
      default:
        return { iconSize, fontSize };
    }
  };

  const getIconImage = (type = "") => {
    switch (type) {
      case "p":
        return regex["Poison"];
      case "d":
        return regex["Damage"];
      case "b":
        return regex["Block"];
      case "o":
        return regex["Pain"];
      case "h":
        return regex["Heal"];
      default:
        return "";
    }
  };

  ret = ret
    // DAMAGE, BLOCK, POISON
    .replaceAll(/\^[dbpho][\+0-9xX\+\-\*]*\^/g, (value) => {
      const type = value.charAt(1);
      const { iconSize, fontSize } = getIcon(value);
      return `
          <div class="iconWithText" style="width:${iconSize}px;height:${iconSize}px;">
            <img class="iconImage ${type}_image" src="${getIconImage(
        type
      )}" width="${iconSize}" height="${iconSize}">
            <span class="textInsideIcon " style="width:${iconSize}px;font-size:${fontSize}px;">
              ${value.slice(2, -1)}
            </span>
            </img>
          </div>
        `;
    })
    // REGEX WORDS to ICONS
    .replaceAll(/\^[a-zA-Z]+\^/g, (value) => {
      const src = regex[value.slice(1, -1)];
      return `
        <span style="position:relative;">
          ${
            !!src
              ? `<img src="${regex[value.slice(1, -1)]}" width="${
                  iconSize + iconSize * 0.3
                }" height="${iconSize + iconSize * 0.3}" />`
              : ""
          }
          ${
            !src
              ? `
            <span style="text-align:center;color:red;font-size:${fontSize}px;line-height:0px;">
              ${value.slice(1, -1)}
            </span>
          `
              : ""
          }
          
        </span>
      `;
    });
  const retValue = ret.slice(3, -5);
  if (retValue.includes("imagedelivery") && retValue.includes("<img")) {
    // <img src=""https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/dreya_graviticbola/public"">
    const imgIndex =
      retValue.slice(retValue.indexOf("<img") + 4, -1).indexOf(">") +
      retValue.indexOf("<img") + 3;
    return `${retValue.slice(
      0,
      imgIndex - 7 // we subtract seven here to include /public in what gets replaced
    )}/quality=100,width=400" width=400 height=400${retValue.slice(
      imgIndex,
      retValue.length
    )}`;
  } else {
    return retValue;
  }
}

export function getCardDescriptionHtml({ card, regex, fontSize = 12, html = false }) {
  const CARD_WIDTH = 250;

  const desc = `<div style='width:${CARD_WIDTH * 0.85}px;min-height:${
    CARD_WIDTH * 0.001
  }px;font-size:${fontSize}px;text-align:center;font-family:Arial;color:#333;'>${processCardDescriptionMarkdown(
    {
      value: card.Description,
      regex,
      fontSize,
      html,
    }
  )}</div>`;

  const trigger = !!card.TriggerAction
    ? `<div style='margin-top:10%;margin-bottom:10%;width:${
        CARD_WIDTH * 0.85
      }px;min-height:${
        CARD_WIDTH * 0.001
      }px;font-size:${fontSize}px;text-align:center;font-family:Arial;color:#222;
      background-color:#ddd;padding:10% 6% 6%;border-radius:12px;
      border: 1px solid black;position:relative;'>
      <div style='display:flex;align-items:center;justify-content:center;background-color:#ddd;border: 1px solid black;
      border-radius:40px;padding:1% 6%;left:39%;position:absolute;top:-13%'>
      ${processCardDescriptionMarkdown({
        value: `^${card.Trigger}^`,
        regex,
        fontSize,
      })}</div>${processCardDescriptionMarkdown({
        value: card.TriggerAction,
        regex,
        fontSize,
        html,
      })}</div>`
    : "";

  return { desc, trigger };
}

export const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};

export function replaceHtmlValues({ html }) {
  return html;
}