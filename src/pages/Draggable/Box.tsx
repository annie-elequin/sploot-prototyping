import { useDrag } from 'react-dnd'
import Card from '../../components/HtmlCard'
const style = {
  position: 'absolute',
  border: '1px dashed gray',
  backgroundColor: 'white',
  padding: '0.5rem 1rem',
  cursor: 'move',
}
export const Box = ({ id, left, top, hideSourceOnDrag, children, card, regex }) => {
  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: 'card',
      item: { id, left, top },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
    }),
    [id, left, top],
  )
  if (isDragging && hideSourceOnDrag) {
    return <div ref={drag} />
  }
  return (
    <div
      className="box"
      ref={drag}
      // @ts-ignore
      style={{ ...style, left, top }}
      data-testid="box"
    >
      <Card card={card} regex={regex} />
    </div>
  )
}
