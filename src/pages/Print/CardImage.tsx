/* eslint-disable */

import React, { useEffect, useRef, useState } from "react";
import { Stage, Layer, Rect, Text, Image, Line } from "react-konva";
import { Button, FormInput } from "shards-react";
import useImage from "use-image";
import domtoimage from "dom-to-image";
import { classColors } from "../../utility/colors";
import { processCardDescriptionMarkdown } from "../../utility";
import { getClassIconUrl } from "../../components/ClassIcon";

const CARD_WIDTH = 822;
const CARD_HEIGHT = 1122;
const DPI = 300;

export default function CardImage({ data, regex, curPage, clearElements }) {
  const [rows] = useState("2");
  const [col] = useState("4");
  const [desc, setDesc] = useState({});
  const [titles, setTitles] = useState({});
  const [renderReady, setRenderReady] = useState(false);

  const stage = useRef(null);

  useEffect(() => {
    if (data && renderReady) {
      getDescriptions();
    }
  }, [renderReady]);

  useEffect(() => {
    setRenderReady(false);
    clearElements();
  }, [curPage]);

  const download = async () => {
    if (stage.current) {
      // @ts-ignore
      const url = stage.current.toDataURL();
      console.log({ url });
      var link = document.createElement("a");
      link.download = "cards.png";
      link.href = url;
      link.click();
    }
  };

  const getDescriptions = async () => {
    clearElements();

    const idMap = {};
    const promises: any[] = [];
    data.forEach(async (card, i) => {
      const div = document.createElement("div");
      div.id = card.id;
      div.style.width = `${CARD_WIDTH * 0.8}px`;

      const cardScale = parseInt(card.Scale);
      let fontSize = CARD_WIDTH * 0.04;
      fontSize = fontSize + fontSize * (cardScale / 100);

      const desc = `<div class='cardDescriptionText' style='width:${
        CARD_WIDTH * 0.85
      }px;min-height:${
        CARD_WIDTH * 0.001
      }px;font-size:${fontSize}px;'>${processCardDescriptionMarkdown({
        value: card.Description,
        regex,
        fontSize,
        iconSize: 50,
      })}</div>`;

      const trigger = !!card.TriggerAction
        ? `<div style='margin-top:10%;margin-bottom:10%;width:${
            CARD_WIDTH * 0.85
          }px;min-height:${
            CARD_WIDTH * 0.001
          }px;font-size:${fontSize}px;text-align:center;font-family:Arial;color:#222;
      background-color:#ddd;padding:10% 6% 6%;border-radius:25px;
      border: 1px solid black;position:relative;'>
      <div style='display:flex;align-items:center;justify-content:center;background-color:#ddd;border: 1px solid black;
      border-radius:40px;padding:1% 5%;left:39%;position:absolute;top:-13%'>
      ${processCardDescriptionMarkdown({
        value: `^${card.Trigger}^`,
        regex,
        fontSize,
        iconSize: 50,
      })}</div>${processCardDescriptionMarkdown({
            value: card.TriggerAction,
            regex,
            fontSize,
            iconSize: 50,
          })}</div>`
        : "";

      div.innerHTML = `${desc}${trigger}`;
      document.getElementById("root")?.appendChild(div);
      promises.push(
        domtoimage.toPng(div, {
          cacheBust: true,
        })
      );
      idMap[i] = card.id;
    });
    const descriptions = {};
    const result = await Promise.all(promises);
    result.forEach((r, i) => {
      descriptions[idMap[i]] = r;
    });
    setDesc(descriptions);
    await getTitles();
  };

  const getTitles = async () => {
    const idMap = {};
    const promises: any[] = [];
    data.forEach(async (card, i) => {
      const div = document.createElement("div");
      div.id = `title-${card.id}`;
      div.style.width = `${CARD_WIDTH * 0.65}px`;

      const fontSize = CARD_WIDTH * 0.06;

      const title = `<div style='width:${CARD_WIDTH * 0.65}px;min-height:${
        CARD_WIDTH * 0.23
      }px;font-size:${fontSize}px;text-align:center;font-family:Arial;color:#333;'>${processCardDescriptionMarkdown(
        {
          value: card.Title,
          regex,
          fontSize,
          iconSize: 50,
        }
      )}</div>`;
      div.innerHTML = `${title}`;
      document.getElementById("root")?.appendChild(div);
      promises.push(
        domtoimage.toPng(div, {
          cacheBust: true,
        })
      );
      idMap[i] = `title-${card.id}`;
    });
    const newTitles = {};
    const result = await Promise.all(promises);
    result.forEach((r, i) => {
      newTitles[idMap[i]] = r;
    });
    setTitles(newTitles);
  };

  const renderCards = () => {
    const r = parseInt(rows);
    const c = parseInt(col);
    const cardElements: any[] = [];
    let ndx = 0;
    for (let i = 0; i < r; i++) {
      for (let j = 0; j < c; j++) {
        const card = data[ndx];
        if (!card) continue;

        // i is row, j is column
        const xOrigin = j * CARD_WIDTH;
        const yOrigin = i * CARD_HEIGHT;
        const props = { card, regex, xOrigin, yOrigin };
        cardElements.push(
          <>
            <Border {...props} />
            <CardName {...props} url={titles[`title-${card.id}`]} />
            <ClassImage {...props} />
            <Level {...props} />
            {card.Image?.trim().length > 0 && (
              <Img
                url={card.Image}
                x={xOrigin + CARD_WIDTH * 0.3}
                y={yOrigin + CARD_HEIGHT * 0.14}
                width={card.imageWidth ? card.imageWidth : CARD_HEIGHT * 0.28}
                height={CARD_HEIGHT * 0.28}
              />
            )}
            <Img
              url={desc[card.id]}
              x={xOrigin + CARD_WIDTH * 0.068}
              y={yOrigin + CARD_HEIGHT * 0.5}
            />
            <Stats {...props} />
            <TodayDate {...props} />
            <Spark {...props} />
          </>
        );
        ndx++;
      }
    }
    return cardElements;
  };

  return !data ? null : (
    <>
      <h4>Canvas</h4>

      <div style={{ paddingBottom: 48 }}>
        <Button style={{ marginRight: 8 }} onClick={download}>
          Download
        </Button>
        <Button onClick={() => setRenderReady(true)}>Render Cards</Button>
      </div>

      {renderReady && (
        <div
          style={{
            width: parseInt(col) * CARD_WIDTH,
            height: parseInt(rows) * CARD_HEIGHT,
            marginBottom: 100,
            border: "1px solid black",
          }}
        >
          <Stage ref={stage} width={11 * DPI} height={8.5 * DPI}>
            <Layer>{renderCards()}</Layer>
          </Stage>
        </div> 
      )}
    </>
  );
}

const Img = ({ url, ...props }) => {
  const [image] = useImage(url, "Anonymous" as any);
  return <Image image={image} draggable {...props} />;
};

function Border({ card, xOrigin, yOrigin }) {
  return (
    <Rect
      x={xOrigin + CARD_WIDTH * 0.038}
      y={yOrigin + CARD_HEIGHT * 0.028}
      width={CARD_WIDTH - CARD_WIDTH * 0.09}
      height={CARD_HEIGHT - CARD_HEIGHT * 0.07}
      cornerRadius={10}
      strokeEnabled={true}
      strokeWidth={CARD_WIDTH * 0.02}
      stroke={classColors[card.Class] || "#777"}
    />
  );
}

function CardName({ card, xOrigin, yOrigin, url }) {
  return (
    <Img
      x={xOrigin + CARD_WIDTH * 0.15}
      y={yOrigin + CARD_HEIGHT * 0.04}
      fontSize={CARD_WIDTH * 0.06}
      width={CARD_WIDTH * 0.65}
      draggable
      url={url}
    />
  );
}

function ClassImage({ card, xOrigin, yOrigin }) {
  return (
    <Img
      url={getClassIconUrl(card.Class)}
      width={CARD_WIDTH * 0.1}
      height={CARD_WIDTH * 0.1}
      x={xOrigin + CARD_WIDTH * 0.82}
      y={yOrigin + CARD_HEIGHT * 0.06}
      draggable
    />
  );
}

function Level({ card, xOrigin, yOrigin }) {
  return (
    <Text
      text={card.Level}
      x={xOrigin + CARD_WIDTH * 0.85}
      y={yOrigin + CARD_HEIGHT * 0.14}
      fontSize={CARD_WIDTH * 0.06}
      fontVariant="bold"
      draggable
    />
  );
}

function Stats({ card, regex, xOrigin, yOrigin }) {
  return (
    <>
      <Line
        x={xOrigin + CARD_WIDTH * 0.04}
        y={yOrigin + CARD_HEIGHT * 0.83}
        points={[0, 0, CARD_WIDTH * 0.9, 0]}
        stroke="#888"
        strokeWidth={8}
      />
      <Img
        x={xOrigin + CARD_WIDTH * 0.13}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Power"]}
      />
      <Text
        text={card["Power"]}
        x={xOrigin + CARD_WIDTH * 0.23}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
      {/* ******************* */}
      <Img
        x={xOrigin + CARD_WIDTH * 0.42}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Speed"]}
      />
      <Text
        text={card["Speed"]}
        x={xOrigin + CARD_WIDTH * 0.52}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
      {/* ******************* */}
      <Img
        x={xOrigin + CARD_WIDTH * 0.72}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Tactics"]}
      />
      <Text
        text={card["Tactics"]}
        x={xOrigin + CARD_WIDTH * 0.82}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
    </>
  );
}

function TodayDate({ xOrigin, yOrigin }) {
  const d = new Date().toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });
  return (
    <Text
      text={d}
      x={xOrigin + CARD_WIDTH * 0.07}
      y={yOrigin + CARD_HEIGHT * 0.93}
      fontSize={CARD_WIDTH * 0.02}
      draggable
    />
  );
}

function Spark({ card, xOrigin, yOrigin }) {
  return (
    <>
      <Rect
        x={xOrigin + CARD_WIDTH * 0.065}
        y={yOrigin + CARD_HEIGHT * 0.045}
        width={CARD_WIDTH * 0.15}
        height={CARD_WIDTH * 0.15}
        strokeEnabled
        strokeWidth={10}
        stroke={classColors[card.Class] || "#777"}
        cornerRadius={100}
      />
      <Text
        text={card.Spark}
        x={xOrigin + CARD_WIDTH * 0.111}
        y={yOrigin + CARD_HEIGHT * 0.067}
        fontSize={CARD_WIDTH * 0.1}
        fontVariant="bold"
        draggable
      />
    </>
  );
}
