import axios from "axios";
import {
  ifcAbilityRegex,
  fetchRawFileUrl,
  mercs,
  backups,
  ifcKeywords,
} from "./constants";
import Papa from "papaparse";
import { localAbilities, localCompare } from "./localData";
import gameConfig from "../gameConfig"
const { currentVersion } = gameConfig;

const gitlabFetch = axios.create({
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_GITLAB_TOKEN}`,
    'PRIVATE-TOKEN': process.env.REACT_APP_GITLAB_TOKEN
  },
});

// const cloudflareFetch = axios.create({
//   headers: {
//     Authorization: `Bearer ${process.env.REACT_APP_CLOUDFLARE_TOKEN}`,
//   },
// });

const duplicateCardsForQuantity = (abilities) => {
  const allOfThem: any = [];
  abilities.forEach((a) => {
    const quantity = parseInt(a.Quantity);
    for (let i = 0; i < quantity; i++) {
      allOfThem.push(a);
    }
  });
  console.log({allOfThem})
  return allOfThem;
};

export const fetchIfcAbilities = async () => {
  if (process.env.NODE_ENV === "development") {
    console.log({ localAbilities });
    return { nonDuplicatedAbilities: localAbilities, allAbilities: duplicateCardsForQuantity(localAbilities) };
  }
  return new Promise(async (res, rej) => {
    const fetchDataPromises: any[] = [];
    const parseDataPromises: any[] = [];
    let allAbilities: any[] = [];

    const getData = async (url) => {
      return await gitlabFetch.get(url);
    };

    const getCards = async (csv) => {
      return new Promise(async (resolve, reject) => {
        Papa.parse(csv, {
          header: true,
          complete: function (results, file) {
            allAbilities = allAbilities.concat(results.data);
            resolve(null);
          },
        });
      });
    };

    mercs.forEach((m) => {
      fetchDataPromises.push(getData(m));
    });

    const allRawData = await Promise.all(fetchDataPromises);
    allRawData.forEach((mercFile) => {
      parseDataPromises.push(getCards(atob(mercFile.data.content)));
    });

    await Promise.all(parseDataPromises);

    console.log("Finished parsing IFC Abilities!", {
      allAbilities,
    });

    res({ nonDuplicatedAbilities: allAbilities, allAbilities: duplicateCardsForQuantity(allAbilities) });
  });
};


export const fetchIfcCompareAbilities = async () => {
  if (process.env.NODE_ENV === "development") {
    console.log({ localCompare });
    return localCompare;
  }
  return new Promise(async (res, rej) => {
    const fetchDataPromises: any[] = [];
    const parseDataPromises: any[] = [];
    let allCompare: any[] = [];

    const getData = async (url) => {
      return await gitlabFetch.get(url);
    };

    const getCards = async (csv) => {
      return new Promise(async (resolve, reject) => {
        Papa.parse(csv, {
          header: true,
          complete: function (results, file) {
            allCompare = allCompare.concat(results.data);
            resolve(null);
          },
        });
      });
    };

    backups.forEach((m) => {
      fetchDataPromises.push(getData(m));
    });

    const allRawData = await Promise.all(fetchDataPromises);
    allRawData.forEach((mercFile) => {
      parseDataPromises.push(getCards(atob(mercFile.data.content)));
    });

    await Promise.all(parseDataPromises);

    console.log("Finished parsing IFC Abilities FOR COMPARE!", {
      allCompare,
    });

    res(allCompare);
  });
};

export const fetchIfcAbilityRegex = async (link = undefined) => {
  return new Promise(async (res, rej) => {
    const data = await axios.get(link ?? ifcAbilityRegex);
    let count = 0;
    let regexValues: any = {};
    Papa.parse(data.data, {
      step: (row) => {
        // row.data is an array of the column data
        if (count === 0) {
          // it's the header
        } else {
          // @ts-ignore
          regexValues[row.data[0]] = row.data[1];
        }
        count++;
      },
      complete: function () {
        console.log("Finished parsing IFC Regex!", {
          regexValues,
        });
        res(regexValues);
      },
    });
  });
};

export const fetchIfcKeywords = async (link = undefined) => {
  return new Promise(async (res, rej) => {
    const data = await axios.get(link ?? ifcKeywords);
    let count = 0;
    const keywords: any = [];
    Papa.parse(data.data, {
      step: (row) => {
        // row.data is an array of the column data
        if (count === 0) {
          // it's the header
        } else {
          // @ts-ignore
          keywords.push(row.data[0])
        }
        count++;
      },
      complete: function () {
        const filtered = keywords.filter(k => k?.length > 0)
        console.log("Finished parsing IFC Keywords!", {
          keywords: filtered,
        });
        res(filtered);
      },
    });
  });
};

// https://gitlab.com/annie-elequin/ifctools/-/blob/main/nathan/Cards/OverheadSlam.txt
// /projects/:id/repository/commits?path=:file_path
// curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/ifctools/repository/commits"
//

export const getCommitsForCard = async (cardId) => {
  const path = `${currentVersion}/Cards/${cardId}.txt`;
  const results = await gitlabFetch.get(
    `https://gitlab.com/api/v4/projects/24709529/repository/commits?path=${path}`
  );
  return results.data;
};

// https://gitlab.com/annie-elequin/ifctools/-/raw/78ebeb95630fb4671e774b26b1659b813a8fe267/nathan/Cards/(Bot)LaserTurret.txt
export const getCardDataAtCommit = async (commit, cardId) => {
  const path = encodeURIComponent(`${currentVersion}/Cards/${cardId}`);
  const ref = `?ref=${commit}`;
  return gitlabFetch.get(`${fetchRawFileUrl}${path}%2Etxt/raw${ref}`);
  // return gitlabFetch.get(`${fetchRawFileUrl}${path}${ref}`);
};

export const uploadImageToCloudflare = async ({ file, id }) => {
  // curl --request POST \
  // https://api.cloudflare.com/client/v4/accounts/<ACCOUNT_ID>/images/v1 \
  // --header 'Authorization: Bearer <API_TOKEN>' \
  // --form 'file=@./<PATH_TO_YOUR_IMAGE>' \
  // --form 'id=<PATH_TO_YOUR_IMAGE>'

  // const accountId = process.env.REACT_APP_CLOUDFLARE_ACCOUNT_ID;

  // const requestUrl = `https://api.cloudflare.com/client/v4/accounts/${accountId}/images/v2/direct_upload`
  // const requestData = new FormData();
  // requestData.append('id', id);

  // const requestResponse = await cloudflareFetch.post(requestUrl, requestData);
  // const requestResponse = await fetch(requestUrl, {
  //   method: 'post',
  //   headers: {
  //     Authorization: `Bearer ${process.env.REACT_APP_CLOUDFLARE_TOKEN}`,
  //     'Access-Control-Allow-Origin': '*',
  //   },
  //   body: requestData,
    
  // })

  // const url = `https://api.cloudflare.com/client/v4/accounts/${accountId}/images/v1`;
  const data = new FormData();
  data.append("id", id);
  data.append("file", file);
  console.log('Uploading image to cloudflare...', {id, file})
  // const response = await cloudflareFetch.post(url, data);
  // const response = await fetch(url, {
  //     method: 'post',
  //     headers: {
  //       Authorization: `Bearer ${process.env.REACT_APP_CLOUDFLARE_TOKEN}`,
  //     },
  //     body: data,
  //     mode: 'no-cors'
  // })
};
