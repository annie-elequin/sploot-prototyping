import {
  Navbar,
  Nav,
  NavItem,
} from "shards-react";
// @ts-ignore
import { Link, useLocation } from "wouter";

export default function Header() {
  const [location,] = useLocation();
  const styles = { color: "white",  padding: 8, borderRadius: 8 }
  return (
    <Navbar type="dark" theme="primary" expand="md">
      <Nav navbar>
        <NavItem style={{ marginRight: 24 }}>
          <Link href="/" style={{ fontWeight: "bold", ...styles }}>
            Interstellar Fight Club
          </Link>
        </NavItem>
        <NavItem style={{ marginRight: 12 }}>
          <Link href="/print" style={{ ...styles, backgroundColor: location === '/print' ? '#00000020' : 'transparent' }}>
            Print
          </Link>
        </NavItem>
        <NavItem style={{ marginRight: 12 }}>
          <Link href="/cards" style={{ ...styles, backgroundColor: location === '/cards' ? '#00000020' : 'transparent' }}>
            View
          </Link>
        </NavItem>
        <NavItem style={{ marginRight: 12 }}>
          <Link href="/compare" style={{ ...styles, backgroundColor: location === '/compare' ? '#00000020' : 'transparent' }}>
            Compare
          </Link>
        </NavItem>
        <NavItem style={{ marginRight: 12 }}>
          <Link href="/history" style={{ ...styles, backgroundColor: location === '/history' ? '#00000020' : 'transparent' }}>
            History
          </Link>
        </NavItem>
        {/* <NavItem style={{ marginRight: 12 }}>
          <Link href="/draggable" style={{ color: "white" }}>
            Sort
          </Link>
        </NavItem> */}
        {/* <NavItem style={{ marginRight: 12 }}>
          <Link href="/handsimulator" style={{ color: "white" }}>
            Simulate
          </Link>
        </NavItem> */}
        <NavItem style={{ marginRight: 12 }}>
          <Link href="/tts" style={{ ...styles, backgroundColor: location === '/tts' ? '#00000020' : 'transparent' }}>
            TTS
          </Link>
        </NavItem>
        {/* <NavItem style={{ marginRight: 12 }}>
          <Link href="/embed" style={{ color: "white" }}>
            Embed
          </Link>
        </NavItem> */}
        <NavItem
          style={{ marginRight: 12, opacity: .5 }}
          onClick={() => {
            localStorage.clear();
            window.location.reload();
          }}
        >
          <div style={{ color: "white", cursor: "pointer" }}>Clear Storage</div>
        </NavItem>
      </Nav>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "flex-end",
          color: "white",
        }}
      >
        v1.2
      </div>
    </Navbar>
  );
}
