Title
Cover Bound


Class
Azura


Level
2


Type
Card


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-KbTK5wN/0/31a57b40/O/Cover%20Bound.png


Quantity
1


Spark
1


Description
^Jump^ **1-3** in a **Line** <br/> If you land next to an **Obstacle**, ^Cast^ **Cast 1**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
azu34


