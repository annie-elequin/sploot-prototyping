Title
^Arrow^Basic Arrow


Class
Dreya


Level
0


Type
Card


Scale
40


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-z7VTdBL/0/b6edb976/O/download%20%2814%29.png


Quantity
2


Spark
1


Description
(^Lob^Lob 3/3): ^d3^


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre3


