Title
Rooftop Bound


Class
Azura


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-rG6fJH9/0/8f41304f/O/icon_3758690_edited.png


Quantity
1


Spark
1


Description
^Jump^ **2-6** in a **Line** until the first unoccupied space after an **Obstacle** <br/>(must pass at least 1 **Obstacle**)


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
azu16


