Title
Daybreak


Class
Dreya


Level
0


Type
Card


Scale
20


Image
https://cdn-icons-png.flaticon.com/512/171/171145.png


Quantity
1


Spark
1


Description
^rushX^Rush until your ^Initiative^Initiative is equal to 0 or the previous Enemy wave


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre7


