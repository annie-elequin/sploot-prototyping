Title
Spark Charge


Class
Scourge


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-dBjqGd5/0/aa920428/O/download%20%2810%29.png


Quantity
1


Spark
0


Description
^Tuck^**Tuck 1** <br/>Lose X **HP** equal to the <br/> ^Spark^ of the card **Tucked** <br/> <br/>^Next^**Next Card**: Increase the Dmg by ^dX^ equal to the ^Spark^ of this **Stack**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco12


