Title
Poison Tip


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://wiki.melvoridle.com/images/thumb/3/3d/Poison_Arrows_%28item%29.png/250px-Poison_Arrows_%28item%29.png


Quantity
1


Spark
0


Description
The ^Next^**Next** card you play will swap all ^d^ Damage values for ^p^ **Poison** values


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre18


