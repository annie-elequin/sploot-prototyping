Title
^Arrow^Bouncing Arrow


Class
Dreya


Level
1


Type
Card


Scale
20


Image
https://www.customerparadigm.com/wp-content/uploads/2019/11/bounce-icon.png


Quantity
1


Spark
1


Description
(^Lob^Lob 3/3): ^d3^ <br/><br/> On ^Hit^Hit: <br/>Starting from the Hit Target, <br/>(^Lob^2/2): ^d2^


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
drec


