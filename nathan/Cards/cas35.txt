Title
Viscious Charisma


Class
Cassius


Level
3


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-X4N4pRk/0/de42edea/O/download%20%2834%29.png


Quantity
1


Spark
2


Description



Keep
FALSE


Trigger
Next


TriggerAction
Play a **Stab** and Choose two adjacent **Enemies** within ^Range^ **3** <br/> They both will perform all actions of this **Stab** card targeting each other


Print
NO


Public
YES


id
cas35


