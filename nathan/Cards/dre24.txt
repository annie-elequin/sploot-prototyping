Title
^Arrow^Soaring Arrow


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-sc25vHj/0/ff57d565/O/arrow-hunting-weapon-icon.png


Quantity
1


Spark
1


Description
(^Lob^Lob 3/3): ^dX^ equal to double your Distance from the Target <br/><br/>Increase this card's Range by ^spX^


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre24


