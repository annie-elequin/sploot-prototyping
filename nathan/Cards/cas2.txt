Title
Cover of Night


Class
Cassius


Level
P


Type
Power


Scale
10


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Cassius/Cassius-Card-Images/i-XNpg6Xp/0/36959868/O/Cover%20of%20NIght.png


Quantity
1


Spark
---


Description



Keep
FALSE


Trigger
Always


TriggerAction
If you are in the adjacent space behind an Enemy when they <br/>^Look^Look or ^Seek^Target,<br/><br/>^Hide^Hide:<br/>they will not detect you


Print
NO


Public
YES


id
cas2


