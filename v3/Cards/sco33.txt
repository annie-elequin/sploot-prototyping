Title
Pocket Lighter


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_pocketlighter/public


Quantity
1


Spark
1


Description
Lose 1-3 HP<br/>Gain 2^Flex^Flex


Keep
FALSE


Trigger
Reaction


TriggerAction
If your current ^Initiative^Initiative is equal to your HP,<br/>^Reclaim^Reclaim this Card


Print
NO


Public
YES


id
sco33


