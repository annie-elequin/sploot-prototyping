Title
Shedding Skin


Class
Foulborn


Order
X


Level



Scale
0


Image
https://www.pngrepo.com/png/320192/180/swirled-shell.png


Quantity
1


Spark
1


Cost
T


Power
1


OnSlot
Lose 2-3 ^b^ Block
^Move^Move 0-2


OnUse
If your ^b^ Block is 5 or less, apply 
<br/>^pX^Poison an adjacent Enemy equal to your Block
<br/>
<br/>^SparkOne^= Apply this ^p^ Poison to another adjacent Enemy


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
fou29a


Speed
0


Tactics
0


