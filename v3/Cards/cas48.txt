Title
Terrorwing


Class
Cassius


Order
31


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/CassiusTerrorwing/public


Quantity
1


Spark
2


Cost
1


Power
2


OnSlot



OnUse
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-P79gBCp/0/c5f428e1/O/Cassius_Terrorwing.png" width="400" height="400">
<br/>You must be on a Platform to Use this card
<br/>Deal ^d5^ Damage to all Targets and then ^Scramble^^Jump^ 3 with Jump


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas48


Speed
0


Tactics
0


