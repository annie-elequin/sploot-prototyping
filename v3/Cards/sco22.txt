Title
Pinball Panda


Class
Scourge


Order
16


Level



Scale
0


Image
https://www.bluetrack.com/uploads/items_images/panda-ball-stress-balls1.jpg


Quantity
1


Spark
1


Cost



Power
1


OnSlot



OnUse
^Move^Move 2-3 spaces in a ^Line^Line
<br/>
<br/>(Range^Range^1): Deal ^dX^ Damage equal to the distance you traveled
<br/>
<br/>^SparkOne^= Repeat these actions
<br/>^SparkOne^= Repeat these actions


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco22


Speed
0


Tactics
0






