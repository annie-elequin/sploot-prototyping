Title
Terrorizer


Class
Scourge


Order
12


Level



Scale
0


Image
https://cdn2.iconfinder.com/data/icons/wild-animals-insects-and-pest-problems-at-house/343/animal-insect-problem-1-1024.png


Quantity
1


Spark
1


Cost



Power
1


OnSlot



OnUse
^Disarm^Disarm, then ^Taunt^Taunt an Enemy within Range^Range^2
<br/>
<br/>Control all of their Movement


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco16


Speed
0


Tactics
0






