Title
Clothesline


Class
Foulborn


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-6ZbxtNV/0/ab881718/O/lariat%20copy.png


Quantity
1


Spark
2


Cost
PS


Power
2


OnSlot



OnUse
^MoveLine^Move up to (2+^SparkX^)
<br/>spaces in a Line
<br/>
<br/>Deal ^d3^ Damage and ^Wound^Wound to each Enemy that was ever adjacent to a space you entered


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
fou26


Speed
0


Tactics
0


