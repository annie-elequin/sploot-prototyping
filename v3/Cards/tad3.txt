Title
(Bot) Laser Turret


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://cdn.icon-icons.com/icons2/390/PNG/512/tesla-turret_38799.png


Quantity
1


Spark
0


Description
^KeepTap^**Keep** = ^Tuck^**Tuck 1** <br/> (Summon) **3** Block, 0 HP, **X** ATK (R3, Line)


Keep
TRUE


Trigger
Tuck


TriggerAction
You may command this Summon to deal ^dX^ equal to half the^Spark^in this Stack. If this damage dealt is **6** or more, immediately ^Cast^ this **Stack**into your **Discard** pile.


Print
NO


Public
NO


id
tad3


