Title
Distraction


Class
Cassius


Order
23


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-WCQHfqZ/0/ccc68000/O/11017253.png


Quantity
1


Spark
0


Cost
0


Power
1


OnSlot



OnUse
Choose an Ally or Platform within (Range^Range^4) to perform:
<br/>
<br/>^Taunt^Taunt all Enemies within 
<br/>(1+^SparkX^) spaces
<br/>
<br/>Each time an Enemy attacks an Ally, ^Wound^Wound them


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas27


Speed
0


Tactics
0


