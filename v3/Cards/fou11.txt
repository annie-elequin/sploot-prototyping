Title
Takedown


Class
Foulborn


Order
12


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-cXtCXVw/0/73476515/O/Foulborn_takedown.png


Quantity
1


Spark
1


Cost
P


Power
1


OnSlot



OnUse
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-WrvqBq4/0/295e7374/O/Foulborn_ShieldBash%20%284%29.png" width="400" height="400"><br/>If you have more ^b^Block than the Target, deal ^dX^^Pierce^Piercing damage equal to their ^b^Block


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou11


Speed
0


Tactics
0


