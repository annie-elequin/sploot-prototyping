Title
Guardian


Class
Foulborn


Order
21


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/FoulbornGuardian/public


Quantity
1


Spark
1


Cost



Power
1


OnSlot
^Move^Move 0-3 towards any Ally
<br/> Choose an adjacent Unit to Gain ^b3^Block


OnUse



Reaction
You have ^b0^ Block


ReactionAction
Any Ally may give you up to ^b3^Block, which you may double


Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou44


Speed
0


Tactics
0


