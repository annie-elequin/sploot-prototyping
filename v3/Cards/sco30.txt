Title
Love Bomb


Class
Scourge


Order
30


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_lovebomb/public


Quantity
1


Spark
1


Cost



Power
0


OnSlot
Summon a 1 HP Ally within (Range^Range^4) with a Max HP of 8


OnUse
Explode this summon, dealing ^dX^ to all adjacent enemies equal to its current HP


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
sco30


Speed
0


Tactics
0






