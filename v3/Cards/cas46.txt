Title
Up My Sleeve


Class
Cassius


Order
30


Level



Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-GW2NVtj/0/931bfbe1/O/Cassius_upmysleeve.png


Quantity
1


Spark
1


Cost
3


Power
2


OnSlot



OnUse
^Draw^Draw cards until your hand has at least (1+^SparkX^) total Cost


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas46


Speed
0


Tactics
0


