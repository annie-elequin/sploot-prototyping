Title
Rallying Cry


Class
Foulborn


Order
14


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-MS2dVGb/0/3d6d6a95/O/2749301.png


Quantity
1


Spark
2


Cost
T


Power
2


OnSlot
^Move^Move 0-2
<br/>All Allies may ^Move^Move 0-2 towards your location


OnUse
Up to ^SparkX^ different Players may gain up to ^b3^Block
<br/>
<br/>Gain all ^b^ Block they decline


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou13


Speed
0


Tactics
0


