Title
Frogged Leap


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://thumbs.dreamstime.com/b/jumping-frog-icon-jumping-frog-icon-vector-design-160323337.jpg


Quantity
1


Spark
1


Description
^Jump^ exactly **3** spaces in a Line.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad5


