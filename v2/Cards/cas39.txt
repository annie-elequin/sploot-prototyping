Title
Secret Stash


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-XRpHg42/0/c3792f63/O/Secret%20Stash.png


Quantity
1


Spark
1


Description
Place up to 3 ^Power^^Speed^^Tactics^Tokens from your supply on this card


Trigger
Always


TriggerAction
Round Start: ^Reclaim^Reclaim this card and its Tokens


Print
NO


Public
YES


id
cas39


Power
0


Speed
0


Tactics
0






