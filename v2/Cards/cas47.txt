Title
Vault


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-tVkDjxP/0/0516cf69/O/Vault.png


Quantity
1


Spark
2


Description
^Jump^Jump (1+^SpeedX^) and ^Draw^Draw X equal to the final distance traveled


Trigger



TriggerAction



Print
NO


Public
YES


id
cas47


Power
0


Speed
2


Tactics
0






