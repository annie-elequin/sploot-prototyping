Title
Rendezvous


Class
Cassius


Level
2


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-NqWwPLW/0/eab36d7d/O/Rendezvous.png


Quantity
1


Spark
1


Description
^Discard^Discard up to 5 cards<br/>Gain ^Speed^ or ^Tactics^ for each card Discarded


Trigger



TriggerAction



Print
NO


Public
NO


id
cas57


Power
0


Speed
0


Tactics
1






