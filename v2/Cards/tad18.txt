Title
Dumpster Dive


Class
Tad


Level
2


Type
Card


Scale
0


Image
https://static.thenounproject.com/png/171890-200.png


Quantity
2


Spark
1


Description
^Jump^ **2**. If you are now next to an **Obstacle**, destory it and Choose **1** from your **Discard** Pile.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad18


