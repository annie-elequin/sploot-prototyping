Title
Twist The Knife


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-qmfmzsP/0/0d4486dc/O/4406294.png


Quantity
1


Spark
1


Description
^Reclaim^Reclaim the top card of your Void Pile if it is a Stab card<br/><br/>Then Gain X ^Power^ equal to its cost


Trigger



TriggerAction



Print
NO


Public
YES


id
cas44


Power
0


Speed
0


Tactics
1






