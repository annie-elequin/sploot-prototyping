Title
Terrorizer


Class
Scourge


Level
1


Scale
0


Image
https://cdn2.iconfinder.com/data/icons/wild-animals-insects-and-pest-problems-at-house/343/animal-insect-problem-1-1024.png


Quantity
1


Spark
1


Description
^Disarm^Disarm, then ^Taunt^Taunt an Enemy within ^Range^2,<br/>while controlling all of their Movement


Trigger



TriggerAction



Print
YES


Public
YES


id
sco16


Power
0


Speed
1


Tactics
1






