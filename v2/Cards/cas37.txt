Title
Searing Shadow


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-rqksm7z/0/8840c4e8/O/download%20%2822%29.png


Quantity
1


Spark
2


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-rW5Fbd7/0/bd555dc8/O/Cassius%20Searing%20Shadow.png" width="400" height="400"> <br/>(!): Must be an Obstacle to play this card<br/><br/>^Blind^Blind all Targets


Trigger



TriggerAction



Print
NO


Public
YES


id
cas37


Power
1


Speed
0


Tactics
0






