Title
Dizzying Stab


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-4dgzVQS/0/3a2835d0/O/download%20%287%29.png


Quantity
1


Spark
1


Description
Target up to (1+^SpeedX^) Enemies within Range (1+^TacticsOne^)<br/><br/>Deal ^d2^ and ^Scatter^Scatter 1 to all Targets


Trigger



TriggerAction



Print
NO


Public
YES


id
cas26


Power
1


Speed
0


Tactics
0






