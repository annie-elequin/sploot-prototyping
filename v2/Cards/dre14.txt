Title
Survivor Skills


Class
Dreya


Level
1


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/37/37818.png


Quantity
1


Spark
1


Description
^Move^Move X equal to the amount of ^Initiative^Initiative until the next Stop


Trigger
Reaction


TriggerAction
^spOne^=^bX^ equal to your current Initiative


Print
YES


Public
YES


id
dre14


Power



Speed



Tactics







