Title
Hardened Scales


Class
Foulborn


Level
P


Type
Power


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-p5PRXxK/0/fa6bb348/O/download%20%289%29.png


Quantity
1


Spark
---


Description
Once per Round: <br/><br/> ^b3^, then repeat for each Enemy within Range (^spX^)


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
fou1


