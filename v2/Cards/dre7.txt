Title
Dusk


Class
Dreya


Level
X


Scale
20


Image
https://cdn-icons-png.flaticon.com/512/171/171145.png


Quantity
1


Spark
1


Description
Clear a Slot and refund up to <br/>2 Tokens on it


Trigger
Reaction


TriggerAction
^Draw^Draw until the total Cost of your Hand exceeds the amount of Tokens in your Supply


Print
NO


Public
NO


id
dre7


Power
0


Speed
0


Tactics
0






