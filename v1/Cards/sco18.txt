Title
Rocket Jump


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/16/16353.png


Quantity
1


Spark
2


Description
^Tuck^Tuck 1<br/>^Jump^Jump X equal to the ^Spark^Spark of the Tucked card<br/><br/>(^Range^2,^Target^All): ^dX^ equal to the distance you ^Jump^Jumped


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco18


