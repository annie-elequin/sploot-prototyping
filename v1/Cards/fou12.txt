Title
Shedding Skin


Class
Foulborn


Level
1


Type
Card


Scale
10


Image
https://www.pngrepo.com/png/320192/180/swirled-shell.png


Quantity
1


Spark
1


Description
Lose 2-4 ^b^ Block and ^hX^Heal an equal amount


Keep
FALSE


Trigger
Reaction


TriggerAction
If your Block is 4 or less, <br/>^pX^ to all adjacent Enemies equal to your ^b^ Block


Print
YES


Public
YES


id
fou12


