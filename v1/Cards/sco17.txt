Title
Combustion


Class
Scourge


Level
0


Type
Card


Scale
0


Image
https://st2.depositphotos.com/47577860/47005/v/600/depositphotos_470053942-stock-illustration-burning-combustion-fire-icon-outline.jpg


Quantity
2


Spark
1


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-MhxNRGz/0/076c248d/X2/AOE%20Designs%20%281%29-X2.png" width="400" height="400"> <br/>^spOne^ = ^d+1^ Damage,<br/>^Scatter^Scatter 1 to all Targets


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco17


