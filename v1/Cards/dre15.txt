Title
Anchor Arrow


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-XfvSHGc/0/dfce0354/O/Anchor%20Arrow.png


Quantity
1


Spark
2


Description
Mark your ^Initiative^Initiative (before playing this card) with a Token. <br/> Gain ^b4^ and ^Pin^**Pin** yourself for the Round


Keep
FALSE


Trigger
Reaction


TriggerAction
(^Lob^2/2): ^dX^ equal to the total ^Initiative^ that has passed since you played this card


Print
NO


Public
YES


id
dre15


