Title
Sauna Spree


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_saunaspree/public


Quantity
1


Spark
0


Description
You and any Players may ^waitX^ Wait until the end of this Phase<br/>(minimum of Wait 1)<br/><br/>Each Player ^hX^Heals X equal to double highest amount of ^Initiative^Time that a single Player ^waitX^Waited


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco25


