Title
Steadfast


Class
Foulborn


Level
1


Type
Card


Scale
25


Image
https://cdn-icons-png.flaticon.com/512/10/10747.png


Quantity
1


Spark
1


Description
<br> <img src="https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/foulborn_steadfastAOE/public"> <br/> ^bX^= ^b3^ for each Enemy you ^Taunt^Taunted


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
fou18


