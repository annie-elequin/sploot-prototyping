Title
Venomous Bite


Class
Foulborn


Level
1


Type
Card


Scale
20


Image
https://www.pngrepo.com/png/320101/180/fangs.png


Quantity
1


Spark
1


Description
(^Range^1): ^d4^ <br/><br/>^hX^ Heal equal to the <br/>^p^Poison now on the target


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
fou19


