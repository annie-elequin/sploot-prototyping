Title
Cross Chop


Class
Foulborn


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-BfGcKmr/0/29e4578a/O/download%20%2814%29.png


Quantity
1


Spark
2


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/AOE-Images-for-Cards/i-gmpZ5fV/0/47d8636a/O/Foulborn%20Cross%20Chop.png" alt="Foulborn-Tail-Lash" border="0" width="300" height="300"> <br/>Deal ^d5^ <br/>Divvy as evenly as possible across all Targets <br/><br/>^spOne^=^d+2^


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou9


